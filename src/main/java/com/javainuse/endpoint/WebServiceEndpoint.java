package com.javainuse.endpoint;

import com.javainuse.generatedSources.InputSOATest;
import com.javainuse.generatedSources.ObjectFactory;
import com.javainuse.generatedSources.OutputSOATest;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class WebServiceEndpoint {

    private static final String NAMESPACE_URI = "http://javainuse.com";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "inputSOATest")
    @ResponsePayload
    public OutputSOATest hello(@RequestPayload InputSOATest request) {

        System.out.println("Hey WebServiceEndpoint + " + request.getTest());

        String outputString = "Hello " + request.getTest() + "!";

        ObjectFactory factory = new ObjectFactory();
        OutputSOATest response = factory.createOutputSOATest();
        response.setResult(outputString);

        return response;

    }

}
